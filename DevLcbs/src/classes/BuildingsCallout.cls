public class BuildingsCallout
{
    public string BuildingName{get;set;}
    public string BuildingId{get;set;}

 public List<Building__c> geBuildings()
 {
    azureactiveDirectoryHelper activeDirectoryHelper = new azureactiveDirectoryHelper();
    String authorizationHeader = activeDirectoryHelper.getLcbsWebApiAccessToken();
    HttpRequest req = new HttpRequest();
    HttpResponse res = new HttpResponse();
    Http http = new Http();
    Building__c B = new Building__c();
    B.Name= 'Charles Building';
    B.Id= 'a02g000000CAHSQ';
    string addBuildingJsonRequest = JSON.serialize(B);
    req.setHeader('Authorization', 'Bearer ' +  authorizationHeader); 
    req.setHeader('Content-Type', 'application/json');
    
    req.setHeader('Content-Type', 'application/json');
    
    
    req.setEndpoint('https://nsaagbldevlcbsweb.azurewebsites.net/api/models/{companyId}/buildings/add');
    req.setMethod('POST');
    
    //these parts of the POST you may want to customize
    req.setCompressed(false);
    req.setBody(addBuildingJsonRequest);
    //Http http = new Http();
    res= http.send(req);
    String buildingRes = res.getBody();
    List<Building__c> buildings = (List<Building__c>) JSON.deserialize(buildingRes, List<Building__c>.class);
    System.debug(buildings);
    return buildings;
 }   
 }