public class LCBS_APILogs
{
public LCBS_RequestResponse_Log__c logs{get;set;}
 public LCBS_APILogs()
 {
     
 }
 public static void captureLogs(String Req, String Res, Integer Status)
 { 
  LCBS_RequestResponse_Log__c logs = new LCBS_RequestResponse_Log__c();   
     logs.Name= logs.id;
     logs.Request__c = Req;
     logs.Response__c = Res;
     if(Status == 202)
     logs.Status__c = 'Success';
     else
     logs.Status__c = 'Failed';
     insert logs;
 }
}