@isTest
public with sharing class LCBS_AccountTrigger_Test {  
    public static testmethod void testAccTrigger() {
        Account testacc = new Account(Name='Test Account');
        insert testacc;
        update testacc;
        delete testacc;
        undelete testacc;
    }
}